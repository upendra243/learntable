# Learn Tables
Learning tables from 1 to 12 is a learning objective for Primary School Math Students.

# Installation
- Install python 3.8
- Make a virtualenv and activate it
- Move to `<pth>/learTable`
- Run `python table.py` to play the Game

# How to play
Hello Welcome to Math Practice Program

What's your name?. - Ankur

What is the answer of 9 * 5?
Enter Answer - 45 ( This is prompt for user )

Correct Answer

What to play more?. Y/N - Y

What is the answer of 12 * 6?
Enter Answer - 67

Oops That's wrong.

What to play more?. Y/N - Y

What is the answer of 12 * 4?
Enter Answer - 48

Correct Answer

What to play more?. Y/N - N

Good Bye Ankur