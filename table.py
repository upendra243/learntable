"""
Learning tables from 1 to 12 is a learning objective for Primary School Math Students
Python command line Game for students to practice their Math skills
"""

import csv
import time
from random import randint


def write_csv_log(data):
    """
    Utility function to write log csv file
    :param data: list of list for play history
    :return:
    """
    try:
        with open("names.csv", 'w') as csvfile:
            writer = csv.writer(csvfile)
            header = ["Question", "Answer", "Correct", "Time Taken To Answer"]
            writer.writerow(header)
            writer.writerows(data)
    except Exception as e:
        # TODO handle error
        print(e)


def play_game():
    """A simple game for learning table from 1 to 12"""

    played_history = []
    play_more = True
    print("Hello Welcome to Math Practice Program\n")
    name = input("What's your name?. - ")
    while play_more:
        history = []
        first_number = randint(1, 12)
        second_number = randint(1, 12)
        expression = f"{first_number} * {second_number}"
        history.append(expression)
        start_time = time.time()
        print(f"\nWhat is the answer of {expression}?")
        result = input("Enter Answer - ")
        history.append(result)
        time_taken = round(time.time() - start_time, 2)
        try:
            answer = int(result)
            if first_number * second_number == answer:
                is_correct = True
            else:
                is_correct = False
        except:
            # TODO handle error
            is_correct = False
        if is_correct:
            history.append("Y")
            print(" \nCorrect Answer")
        else:
            print(" \nOops That's wrong.")
            history.append("N")
        history.append(time_taken)
        played_history.append(history)

        # check if user want to play more
        want_play_more = input("\nWhat to play more?. Y/N -")
        if want_play_more == "Y" or want_play_more == "y":
            play_more = True
        else:
            play_more = False

    write_csv_log(played_history)
    print(f"\nGood Bye {name}")


if __name__ == "__main__":
    play_game()
